package uz.eticket.dumper.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author: Dilsh0d Tadjiev on 05.03.2020 18:00.
 * @project: Electron-Ticket
 * @version: 2.0
 */
@Getter
@Setter
@ToString
public class SnapshotProcessExceptionResult implements Serializable {
    private String snapshotId;
    private String status;
}
