package uz.eticket.dumper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import uz.eticket.dumper.dto.AggregateSnapshotsDoneResult;
import uz.eticket.dumper.dto.DoneSnapshotResult;
import uz.eticket.dumper.dto.SnapshotProcessExceptionResult;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: Dilsh0d Tadjiev on 05.03.2020 15:42.
 * @project: Electron-Ticket
 * @version: 2.0
 */
@Slf4j
@Configuration
@EnableKafka
public class KafkaListenerConfig {

    @Value(value = "${kafka.server.url}")
    private String kafkaServerUrl;

    @Autowired
    private ObjectMapper objectMapper;

    @Bean
    public ConcurrentKafkaListenerContainerFactory<Integer, String> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<Integer, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }

    @Bean
    public ConsumerFactory<Integer, String> consumerFactory() {
        return new DefaultKafkaConsumerFactory<>(consumerConfigs());
    }

    @Bean
    public Map<String, Object> consumerConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServerUrl);
        props.put(
                ConsumerConfig.GROUP_ID_CONFIG,
                "KafkaSnapshotConsumer");
        props.put(
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class);
        props.put(
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class);
        return props;
    }

    @KafkaListener(topics = "snapshot_topic",groupId = "KafkaSnapshotConsumer")
    public void listen1(ConsumerRecord<String, String> event) {
        String eventClassName = new String(event.headers().toArray()[5].value());
        if ("uz.eticket.microservice.snapshot.api.response.AggregateSnapshotsDoneResult".equals(eventClassName)) {
            try {
                AggregateSnapshotsDoneResult aggregateSnapshotsDoneResult = objectMapper.readValue(event.value(), AggregateSnapshotsDoneResult.class);
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------AggregateSnapshotsDoneResult snapshotId: "+aggregateSnapshotsDoneResult.getSnapshotId()+"------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
//            System.exit(0);

        } else if ("uz.eticket.microservice.snapshot.api.response.SnapshotProcessExceptionResult".equals(eventClassName)) {
            try {
                SnapshotProcessExceptionResult aggregateSnapshotsDoneResult = objectMapper.readValue(event.value(), SnapshotProcessExceptionResult.class);
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------SnapshotProcessExceptionResult: "+aggregateSnapshotsDoneResult.toString()+"------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            System.exit(0);
        } else if ("uz.eticket.microservice.snapshot.api.response.DoneSnapshotResult".equals(eventClassName)) {
            try {
                DoneSnapshotResult doneSnapshotResult = objectMapper.readValue(event.value(), DoneSnapshotResult.class);
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------Snapshot Process Done: "+doneSnapshotResult.toString()+"------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------------------------------------------------------------");
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            System.exit(0);
        }

        log.info("-----------------------------------------------------------------------------------------------");
        log.info("-----------------------------------------------------------------------------------------------");
        log.info("-----------------------------------------------------------------------------------------------");
        log.info("-----------------------------------------"+eventClassName+"------------------------------------------------------");
        log.info("-----------------------------------------------------------------------------------------------");
        log.info("-----------------------------------------------------------------------------------------------");
        log.info("-----------------------------------------------------------------------------------------------");
    }
}
