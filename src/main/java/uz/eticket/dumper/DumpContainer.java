package uz.eticket.dumper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author: Dilsh0d Tadjiev on 05.03.2020 18:38.
 * @project: Electron-Ticket
 * @version: 2.0
 */
@Slf4j
@Component
public class DumpContainer {

    @Autowired
    private SendCommandMiddleware sendCommandMiddleware;

    @Value("${dump.command}")
    private String dumpCommand;

    public void getDumpPostgres() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("bash","-c",dumpCommand.replaceAll("___","eticket_"+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))));
//        processBuilder.environment().put("PGPASSWORD", "eticket");

        try {
            Process process = processBuilder.start();
            int processComplete = process.waitFor();
            if (processComplete == 0) {
                log.info("-------------------------------------------------------------------------------------------------------------");
                log.info("-------------------------------------------------------------------------------------------------------------");
                log.info("-------------------------------------------------------------------------------------------------------------");
                log.info("-------------------------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------Backup created successfully-----------------------------------------");
                log.info("-----------------------------------------Backup created successfully-----------------------------------------");
                log.info("-----------------------------------------Backup created successfully-----------------------------------------");
                log.info("-----------------------------------------Backup created successfully-----------------------------------------");
                log.info("-------------------------------------------------------------------------------------------------------------");
                log.info("-------------------------------------------------------------------------------------------------------------");
                log.info("-------------------------------------------------------------------------------------------------------------");
                log.info("-------------------------------------------------------------------------------------------------------------");
                sendCommandMiddleware.sendCommandSnapshot();
            } else {
                log.info("-------------------------------------------------------------------------------------------------------------");
                log.info("-------------------------------------------------------------------------------------------------------------");
                log.info("-------------------------------------------------------------------------------------------------------------");
                log.info("-------------------------------------------------------------------------------------------------------------");
                log.info("-----------------------------------------Could not create the backup-----------------------------------------");
                log.info("-----------------------------------------Could not create the backup-----------------------------------------");
                log.info("-----------------------------------------Could not create the backup-----------------------------------------");
                log.info("-----------------------------------------Could not create the backup-----------------------------------------");
                log.info("-------------------------------------------------------------------------------------------------------------");
                log.info("-------------------------------------------------------------------------------------------------------------");
                log.info("-------------------------------------------------------------------------------------------------------------");
                log.info("-------------------------------------------------------------------------------------------------------------");

                String s = null;
                try {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(process.getErrorStream()));
                    while ((s = br.readLine()) != null) {
                        System.out.println("Ошибка "+s);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
                System.exit(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
