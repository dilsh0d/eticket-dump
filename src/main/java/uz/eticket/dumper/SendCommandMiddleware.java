package uz.eticket.dumper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.Collections;

/**
 * @author: Dilsh0d Tadjiev on 05.03.2020 18:53.
 * @project: Electron-Ticket
 * @version: 2.0
 */
@Component
public class SendCommandMiddleware {

    @Value("${snapshot.api.run.code}")
    private String snapshotApiRunCode;

    @Value("${eticket.middleware.server}")
    private String eticketMiddlewareServer;

    @Autowired
    private RestTemplate restTemplate;

    public void sendCommandSnapshot(){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setAcceptCharset(Collections.singletonList(Charset.forName("UTF-8")));

        HttpEntity requestHttpEntity = new HttpEntity<>(headers);

        Boolean responseEntity =
                restTemplate.postForObject((eticketMiddlewareServer + "/api/v1/snapshot/start/" + snapshotApiRunCode), requestHttpEntity, Boolean.class);

        System.out.println(responseEntity);
    }

}
