package uz.eticket.dumper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @author: Dilsh0d Tadjiev on 04.03.2020 16:19.
 * @project: Electron-Ticket
 * @version: 2.0
 */
@Component
public class SendSnapshotEventStartListener /*implements ApplicationListener<ContextStartedEvent>*/ {

    @Autowired
    private DumpContainer dumpContainer;


    @EventListener
    public void onApplicationEvent(ContextStartedEvent event) {
        dumpContainer.getDumpPostgres();
    }
}
